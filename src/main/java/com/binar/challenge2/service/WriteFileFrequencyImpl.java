package com.binar.challenge2.service;

import com.binar.challenge2.data.Calculating;
import com.binar.challenge2.data.ShareListDataToGlobal;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;
import java.util.Set;

public class WriteFileFrequencyImpl implements WriteFileFrequency {

    private final Calculating calculating = new Calculating();


    @Override
    public void writeFileFrequency(String saveFileFrequency) {
        ShareListDataToGlobal data = new ShareListDataToGlobal();
        try {
            File file = new File(saveFileFrequency);
            if(file.createNewFile()){
                System.out.println("File pengelompokan nilai telah berhasil dibuat.");
            }
            FileWriter fw = new FileWriter(file);
            BufferedWriter writer = new BufferedWriter(fw);

            Map<Integer, Integer> map = calculating.frequency(data.getListGrades());
            Set<Integer> arrNilai = map.keySet();

            writer.write("Hasil Pengelompokan Data");
            writer.newLine();
            writer.write("Nilai \t\t\t | \t\t\tFrekuensi");
            writer.newLine();
            for (Integer nilai : arrNilai) {
                writer.write("%d \t\t\t\t | \t\t\t\t%d".formatted(nilai, map.get(nilai)));
                writer.newLine();
            }
            writer.flush();
            writer.close();
            System.out.println("Berhasil membuat File Baru.");
        } catch (IOException e) {
            e.getMessage();
        }
    }
}
