package com.binar.challenge2.service;

import com.binar.challenge2.data.Calculating;
import com.binar.challenge2.data.ShareListDataToGlobal;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class WriteFileMeanMedianModusImpl implements WriteFileMeanMedianModus {

    private final Calculating calculating = new Calculating();

    @Override
    public void writeFileGradesMeMeMod(String saveFileMeanMedianModus) {
        ShareListDataToGlobal data = new ShareListDataToGlobal();
        try {
            File file = new File(saveFileMeanMedianModus);
            if(file.createNewFile()){
                System.out.println("File menghitung Mean, Median, dan Modus telah berhasil dibuat.");
            }
            FileWriter fw = new FileWriter(file);
            BufferedWriter writer = new BufferedWriter(fw);

            writer.write("Hasil Pengolahan Nilai");
            writer.newLine();
            writer.write("Berikut hasil sebaran data nilai:");
            writer.newLine();
            writer.write("Mean: %s".formatted(calculating.mean(data.getListGrades())));
            writer.newLine();
            writer.write("Median: " + calculating.median(data.getListGrades()));
            writer.newLine();
            writer.write("Modus: " + calculating.modus(data.getListGrades()));

            writer.flush();
            writer.close();
            System.out.println("Berhasil Memperbaharui File Baru.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
