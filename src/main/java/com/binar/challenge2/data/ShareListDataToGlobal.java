package com.binar.challenge2.data;

import com.binar.challenge2.service.ReadFileImpl;
import lombok.Getter;

import java.util.List;

@Getter
public class ShareListDataToGlobal {
    private final ReadFileImpl rf = new ReadFileImpl();
    public List<Integer> listGrades = rf.readFile();
}
