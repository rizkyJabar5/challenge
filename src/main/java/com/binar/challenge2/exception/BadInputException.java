package com.binar.challenge2.exception;

import java.util.InputMismatchException;

public class BadInputException extends InputMismatchException {

    public BadInputException(String msg) {
        super(msg);
    }

}