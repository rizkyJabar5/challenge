package com.binar.challenge2.exception;

import java.io.FileNotFoundException;

import static com.binar.challenge2.service.ReadFile.PATH_FILE_READ;

public class ReadWriteFileException extends FileNotFoundException {
    public ReadWriteFileException() {
        super("File tidak ada nih bestie, silahkan dicek di " + PATH_FILE_READ);
    }

    public ReadWriteFileException(String message) {
        super(message);
    }
}
