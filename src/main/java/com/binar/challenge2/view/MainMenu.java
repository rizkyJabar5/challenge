package com.binar.challenge2.view;

import com.binar.challenge2.service.WriteFileFrequency;
import com.binar.challenge2.service.WriteFileFrequencyImpl;
import com.binar.challenge2.service.WriteFileMeanMedianModus;
import com.binar.challenge2.service.WriteFileMeanMedianModusImpl;

public class MainMenu extends Menu{

    @Override
    public void showMenu() {

        WriteFileMeanMedianModus meanMedianModus = new WriteFileMeanMedianModusImpl();
        WriteFileFrequency frequency = new WriteFileFrequencyImpl();
        headerMenu();

        switch (termMenu()) {
            case 0 -> {
                System.out.println("Sampai jumpa kembali.");
                System.exit(0);
            }
            case 1 -> {
                frequency.writeFileFrequency(PATH_FILE_COLLECT);
                System.out.println("File telah di generate di " + PATH_FILE_COLLECT);
                footerMenu();
            }
            case 2 ->{
                meanMedianModus.writeFileGradesMeMeMod(PATH_FILE_RESULT);
                footerMenu();
            }
            case 3 -> {
                frequency.writeFileFrequency(PATH_FILE_COLLECT);
                meanMedianModus.writeFileGradesMeMeMod(PATH_FILE_RESULT);
                System.out.println("File telah di generate di " + TERM_PATH_FILE);
                footerMenu();
            }
            default -> {
                System.out.println("Waaah sepertinya Kamu salah pencet nih. Masukkan kembali dengan benar ya!");
                this.showMenu();
            }
        }
    }
}
