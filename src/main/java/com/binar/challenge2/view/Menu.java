package com.binar.challenge2.view;
import com.binar.challenge2.exception.BadInputException;

import java.util.InputMismatchException;
import java.util.Scanner;

public abstract class Menu {
    //    ----------------Variabel Const Path File--------------------
    public static final String TERM_PATH_FILE = "src/main/resources";
    protected static final String PATH_FILE_COLLECT = "src/main/resources/pengelompokan_data.txt";
    protected static final String PATH_FILE_RESULT = "src/main/resources/hasil_pengolahan_data.txt";

    //    ----------------Show Menu Implemented--------------------
    protected void showMenu() {}

    //    ----------------User Input Define--------------------
    protected int termMenu() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Pilih Menu >>>> ");
        int choice = 0;
        try {
            choice = scanner.nextInt();
        } catch (InputMismatchException e) {
            try {
                throw new BadInputException("Inputan kamu salah bestie. Masukkan angka yaa");
            } catch (BadInputException ex) {
                System.err.println(ex.getMessage());
            }finally {
                this.showMenu();
            }
        }
        return choice;
    }

//    ----------------Header Menu--------------------
    protected void headerMenu(){

        System.out.println("-------------------------------------------");
        System.out.println("Aplikasi Pengolah Nilai Siswa");
        System.out.println("-------------------------------------------");
        System.out.println("File akan dibaca di " + TERM_PATH_FILE);
        System.out.println("File akan di generate di " + TERM_PATH_FILE);
        System.out.println("\nSilahkan pilih Menu :");
        System.out.println("""
                1. Kelompokkan data
                2. Menyajikan mean, median, dan modus
                3. Generate kedua file
                0. Exit""");
    }

    //    ----------------Footer Menu--------------------
    protected void footerMenu() {
        System.out.println("\n\n1. Kembali ke Menu Utama\n0. Exit");
        switch (termMenu()) {
            case 0 -> {
                System.out.println("Sampai jumpa kembali.");
                System.exit(0);
            }
            case 1 -> showMenu();
            default -> {
                System.out.println("Waaah sepertinya Kamu salah pencet nih. Masukkan kembali dengan benar ya!");
                this.showMenu();
            }
        }
    }
}