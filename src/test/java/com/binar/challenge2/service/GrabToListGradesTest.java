package com.binar.challenge2.service;

import com.binar.challenge2.data.ShareListDataToGlobal;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class GrabToListGradesTest {

    ShareListDataToGlobal data = new ShareListDataToGlobal();
    List<Integer> getList = data.getListGrades();
    List<Integer> listOfNull = null ;

    @Test
    @DisplayName("When success to grab List -> Then Write to File")
    void returnWhenListArrayIsNotNull(){

        Assertions.assertFalse(getList.isEmpty());

        try {
            File file = new File("src/test/resources/writelist.csv");
            if (file.createNewFile()) {
                System.out.println("File Telah berhasil dibuat");
            }
            FileWriter fw = new FileWriter(file);
            BufferedWriter writer = new BufferedWriter(fw);
            writer.write("Hasil Pengolahan Nilai");
            writer.newLine();
            for (Integer c : getList ) {
                writer.write("%d".formatted(c));
                writer.newLine();
            }
            writer.flush();
            writer.close();
        }catch(IOException e){
            System.err.println("Gagal menulis File " + e.getMessage());
        }
    }

    @Test
    @DisplayName("Write file is not succeed- When Array is empty")
    void returnWhenListArrayIsNull() {
        Exception e = Assertions.assertThrows(NullPointerException.class, () -> Assertions.assertTrue(listOfNull.isEmpty()));
        e.getCause();
    }
}
