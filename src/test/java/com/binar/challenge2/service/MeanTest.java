                            package com.binar.challenge2.service;

                            import com.binar.challenge2.data.Calculating;
                            import com.binar.challenge2.data.ShareListDataToGlobal;
                            import org.junit.Before;
                            import org.junit.Test;
                            import org.junit.jupiter.api.Assertions;
                            import org.junit.jupiter.api.DisplayName;

                            import java.util.Arrays;
                            import java.util.List;

public class MeanTest {

    Calculating calc = new Calculating();
    ShareListDataToGlobal data = new ShareListDataToGlobal();
    List<Integer> listArr, listOfNull = null;

    private void grabListArray() {
        Integer[] arr = {5, 2, 3, 10, 15, 2, 7, 1, 3, 1, 2, 8, 5, 3, 4, 6, 6, 9, 9, 4, 8};
        listArr = Arrays.asList(arr);
        System.out.println(listArr);
    }

    @Before
    public void  init (){
        grabListArray();
    }

    @Test
    @DisplayName("Mean is right a value")
    public void meanRightResult(){

        double expectedValue = 5.380952380952381;
        Assertions.assertEquals(expectedValue, calc.mean(listArr));
    }

    @Test
    @DisplayName("Mean is list of null")
    public void meanIsWrongResult(){
        Exception e = Assertions.assertThrows(NullPointerException.class, ()-> calc.mean(listOfNull));
        e.getMessage();
    }
}
